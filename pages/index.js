import styles from "../styles/index.module.scss"
import Navbar from "../components/general/Navbar";
import request from "../servicecs/axios";
import Link from 'next/link'
import {MdAddShoppingCart} from "react-icons/md"

export default function Home({products}) {
    return (
        <>
            <Navbar/>
            <main className={styles.main}>
                {products.map(item =>
                    <div key={item.id} className={styles.card}>
                        <img alt={item.title} src={item.image}/>
                        <div className={styles.title}>{item.title}</div>
                        <Link href={`/products/${item.id}`}>
                            <a className={styles.link}>
                                <MdAddShoppingCart/>
                                ADD TO CART
                            </a>
                        </Link>
                    </div>
                )}
            </main>
        </>
    )
}

export async function getServerSideProps({}) {
    try {
        const products = await request('getAllProducts')
        return {
            props: {
                products,
                error: !products
            },
        }
    } catch (err) {
        //error handling
        return {error: true};
    }


}