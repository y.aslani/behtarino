import request from "../../servicecs/axios";
import Head from "next/head";
import ProductDetails from "../../components/products/ProductDetails";
import styles from "../../styles/products/Product.module.scss";


const Product = ({product, error}) => {
    if (error)
        return <div>page not found</div>
    else
        return (
            <>
                <Head>
                    <title>{product.title}</title>
                    <meta charSet="utf-8"/>
                    <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                    <meta key="description" name="description" content={product.description}/>
                </Head>
                <main className={styles.product}>
                    <ProductDetails info={product}/>
                </main>
            </>
        )
}

export default Product


export async function getServerSideProps({params}) {
    try {
        const product = await request('getProductDetails', {id: params.id})
        return {
            props: {
                product,
                error: !product
            },
        }
    } catch (err) {
        //error handling
        return {error: true};
    }


}
