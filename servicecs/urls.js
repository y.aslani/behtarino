const urls = {
    getProductDetails: { url: "products/{id}", method: "get" },
    getAllProducts: { url: "products", method: "get" },
};
export default urls;
