import Axios from "axios";
import urls from "./urls";

const TIMEOUT = 60000;

const instance = Axios.create({
    baseURL: "https://fakestoreapi.com/",
    timeout: TIMEOUT,
});

const request = (name, params = null, payload = null) => {
    return new Promise(function (resolve, reject) {
        instance.request({
            url: generateUrl(name, params),
            method: urls[name].method,
            data: payload,
        })
            .then((data) => {
                // console.log(data.data)
                resolve(data.data);
            })
            .catch((error) => {
                // let err = {
                //     message: error.response.data ? error.response.data.message : "",
                //     status: error.response.status,
                //     code: error.response.data ? error.response.data.code : "",
                // };

                reject(error);
            });
    });
};

const generateUrl = (name, params = {}) => {
    let url = urls[name].url;
    if (params) {
        Object.keys(params).forEach((key, index) => {
            if (params[key]) {
                if (url.indexOf("{" + key + "}") !== -1) {
                    url = url.replace("{" + key + "}", params[key]);
                } else {
                    let prefix = "?";
                    if (url.indexOf("?") !== -1) {
                        prefix = "&";
                    }
                    url += prefix + key + "=" + params[key];
                }
            }
        });
    }
    return url;
}

export default request;