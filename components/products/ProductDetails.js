import styles from '../../styles/products/Product.module.scss'
import Ratings from "../general/Rating";
import {MdShare, MdShoppingCart} from "react-icons/md"

const ProductDetails = ({info}) => {
    const getPrice = () => {
        let price = info.price + 50
        if (price % 1 === 0)
            return price
        else
            return price.toFixed(2)
    }
    return (
        <div className={styles.card}>
            <div className={styles.image}>
                <div className={styles.imageFrame}>
                    <img alt={info.title} src={info.image}/>
                </div>
            </div>
            <div className={styles.info}>

                <div className={styles.title}>
                    <h1>{info.title}</h1>
                    <Ratings rate={info.rating.rate}/>
                </div>

                <div className={styles.category}>
                    {info.category}
                </div>

                <div className={styles.price}>
                    <span>{'$' + getPrice()}</span>
                    <span className="text-red-200">{'$' + info.price}</span>
                </div>

                <div className={styles.description}>
                    <div>DESCRIPTION</div>
                    <div className="text-grey-300">
                        {info.description}
                    </div>
                </div>

                <div className={styles.actions}>
                    <button className={styles.add}>
                        <MdShoppingCart size={15}/>
                        <span>ADD TO CART</span>
                    </button>
                    <button className="text-grey-300">
                        <MdShare size={30}/>
                    </button>
                </div>
            </div>
        </div>
    )
}
export default ProductDetails