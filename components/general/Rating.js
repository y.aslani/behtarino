import {MdStarBorder, MdStarHalf, MdStar} from "react-icons/md"
import styles from "../../styles/general/Ratings.module.scss"

const Ratings = ({rate = 0,color='#FF7A76',size='18'}) => {

    return (
        <div className={styles.ratings}>
            {Array.from({length: Math.floor(rate)}, (_, i) =>
                <MdStar color={color} size={size} key={i}/>
            )}
            {rate % 1 !== 0 ?
                <MdStarHalf color={color}  size={size}/>
                : ''
            }
            {Array.from({length: 5 - Math.ceil(rate)}, (_, i) =>
                <MdStarBorder color={color} key={i}  size={size}/>
            )}
        </div>
    )
}

export default Ratings